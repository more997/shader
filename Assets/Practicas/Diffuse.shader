﻿Shader "More/Diffuse"
{
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_MaskTex("Lighting Mask (RGB)", 2D) = "black" {}
		_bwBlend("Black & White blend", Range(0, 1)) = 0
		_SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
      	_Shininess ("Shininess", Float) = 10
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf DiffuseMask fullforwardshadows //es un Surface Shader.
#pragma target 3.0
#include "UnityCG.cginc"


		sampler2D _MainTex;
		sampler2D _MaskTex;
		float _bwBlend;
		struct Input {
		float2 uv_MainTex;
		float2 uv_MaskTex;
		float4 _SpecColor; 
        float _Shininess;
	};
            
	struct SurfaceOutputMask
	{
		float3 Albedo;
		float3 Normal;
		float3 Emission;
		float Specular;
		float Gloss;
		float Alpha;
		float Mask;

	};

	float4 LightingDiffuseMask(SurfaceOutputMask s, float3 lightDir,  float3 viewDir, float atten) {
		float NdotL = saturate(dot(s.Normal, lightDir)); //diffuse
		float4 c;
		c.rgb = lerp(s.Albedo * _LightColor0.rgb * (NdotL * atten * 2),s.Albedo,s.Mask); //interpola (desde,hasta,por)
		c.a = s.Alpha;
		return c;
	}

	float4 _Color;

	void surf(Input IN, inout SurfaceOutputMask o) {
		float4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		float m = tex2D(_MaskTex,IN.uv_MaskTex).g;
		float lum = c.r*.3 + c.g*.59 + c.b*.11;
		float3 bw = float3(lum, lum, lum);
		o.Albedo = lerp(c.rgb,bw, _bwBlend);
		o.Mask = m;
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}