﻿
Shader "More/Specular" {
    Properties {
        _Color ("Color (RGB)", Color) = ( 1, 1, 1, 1 )
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _SpecColor ("Specular Color", Color) = (0.5,0.5,0.5,1)
        _SpecularMap ("Specular Texture", 2D) = "white" {}
        _Shininess ("Shininess", Range (0.01, 1)) = 100
        _BumpMap("Bump Map", 2D) = "bump" {}
        _Cube ("Reflection Cubemap", Cube) = "" { TexGen CubeReflect }
        _ReflectColor("Reflection Color", Color) = (1,1,1,0.5)
        _FresnelPow("Fresnel Power", Range(0, 1)) = 0.25
        _bwBlend("Black & White blend", Range(0, 1)) = 0
        _Dithering("Dithering",2D)= "white" {}
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 400
       
        CGPROGRAM
        #pragma surface surf BlinnPhong
        #pragma target 3.0
 
        // ------------------------------------------
        // Variables from the properties inspector --
        sampler2D _MainTex;
        sampler2D _SpecularMap;
        samplerCUBE _Cube;
        sampler2D _BumpMap;
       	float _bwBlend;
       	sampler2D _Dithering;
        fixed4 _Color;
        fixed  _Shininess;
        fixed4 _ReflectColor;
       
        fixed _FresnelPow;
       
        half4 _GlobalSpecularPos;
        // ------------------------------------------
   
        struct Input {
            float2 uv_MainTex;
            float3 worldRefl;
            float3 viewDir;
            float3 worldPos;
            INTERNAL_DATA
        };
 
        void surf (Input IN, inout SurfaceOutput o) {  
       
            // Main Texture sampling --    
            fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
            tex.rgb *= _Color.rgb;
            // ------------------------
           
            // Sample the normal from bumpmap
            fixed3 normalDir = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
            o.Normal = normalDir;
            // ------------------------
                       
            // Specular calculations
            fixed4 specMap = tex2D(_SpecularMap, IN.uv_MainTex);
            fixed3 lightDirSpec = normalize( _GlobalSpecularPos.xyz - IN.worldPos );
            fixed3 viewDirSpec = normalize( _WorldSpaceCameraPos.xyz - IN.worldPos );
                    half3 reflection = normalize(2 * (( dot( normalDir, lightDirSpec )) ) * normalDir - lightDirSpec);
            half3 specular = pow( dot( reflection, viewDirSpec ), _Shininess ) * _SpecColor.rgb;
            specular = specular * specMap.rgb;
            // ------------------------
           
            // Calculate fresnel for amount of reflection
            fixed3 viewDir = normalize( IN.viewDir );
            half fresnelVal = 1 - saturate(dot(viewDir, normalDir));
            fresnelVal = pow( fresnelVal, _FresnelPow );
            // ------------------------
               
            // Sample the cubemap
            float3 worldRefl = WorldReflectionVector (IN, o.Normal);
            fixed4 reflcol = texCUBE (_Cube, worldRefl) * specMap * fresnelVal;
            // ------------------------
           	float lum = tex.r*.3 + tex.g*.59 + tex.b*.11;
           	float bw = (lum,lum,lum);
            o.Albedo = lerp(tex.rgb,bw, _bwBlend);
            o.Gloss = specMap.r;
            o.Specular = _Shininess;
            o.Emission = reflcol.rgb * _ReflectColor.rgb;
            o.Alpha = tex.a;
        }
        ENDCG
    }
    FallBack "Reflective/Bumped Diffuse"
}
 