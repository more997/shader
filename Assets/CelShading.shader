﻿Shader "Custom/CelShading" {
	Properties {
		_Color("Color", Color) = (1, 1, 1, 1) //color del material
		_MainTex("Albedo (RGB)", 2D) = "white" {} //textura del material
		_NdotLBlend("Shadows blend", Range(0, 0.5)) = 0 //elige el tono de la sombra, le pongo un limite para que no sea completamente oscuro.
		_bwBlend("Black & White blend", Range(0, 1)) = 0 //Saturado o Desaturado
		//_Dithering("Dithering",2D)= "white" {} //Textura Puntillismo sin implementar.
		_LightBlend("Light blend", Range(1, 5)) = 1 //elige el tono de la luz. A partir de 6 se volvia muy claro
		_ShadowRange("Shadow Range", Range(-1, 1)) = 0 //Rango de la luz y sombra.
		_Rgrey("R grey", Range(0,1)) = 0
		_Ggrey("G grey", Range(0,1)) = 0
		_Bgrey("B grey", Range(0,1)) = 0
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}
		LOD 200

		CGPROGRAM
		#pragma surface surf CelShading //como trabajo con las luces, me resulto mas optimo trabajar con un surface shader
		#pragma target 3.0 //texture lod sampling 10 interpolaciones, me permite mas intrucciones de matematica y textura. 
		sampler2D _MainTex;
		fixed4 _Color;
		float _NdotLBlend;
		float _bwBlend;
		float _LightBlend;
		float _ShadowRange;
		float _Rgrey;
		float _Ggrey;
		float _Bgrey;
       	//sampler2D _Dithering;

		half4 LightingCelShading(SurfaceOutput s, half3 lightDir, half atten) { //aca implemento el cel shading.
			half NdotL = dot(s.Normal, lightDir) - _ShadowRange ; //saco el producto punto entre la normal y la direccion de la luz
			if (NdotL <= 0.0) 
				NdotL = _NdotLBlend; //Sombras si es menor o igual a 0.0 es negro.
			else NdotL = 1; // 1 = Luz.
				half4 c; //RGBA
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * _LightBlend);  // toma el color de la luz, implementa el cel shader. 
			c.a = s.Alpha;      //Sobre _LightBlend, el numero que habia antes era un 2 y se veia muy brillante.
			return c;
		}
		struct Input {
			float2 uv_MainTex; 
		};

		void surf(Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color; //le pongo la textura del modelo y el color elegido.
			//float lum = c.r*.3 + c.g*.59 + c.b*.11; //calculo los colores de la textura+color para implementar el blanco y negro. deber ser igual a 300.
			float lum = c.r*_Rgrey + c.g*_Ggrey + c.b*_Bgrey;
			float3 bw = float3(lum, lum, lum); //bw me da la variable para que sea desaturado
			o.Albedo = lerp(c.rgb,bw, _bwBlend); //calculo los colores de C, el calculo de blanco y negro y cuanto quiero implementar del blanco y negro;
			o.Alpha = c.a; 
			}
		ENDCG
	}
	FallBack "Diffuse"
} 
